#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "global.h"

void rng(struct msgbuf *param){
	param->time = rand()%MAX_TIME;
	param->len = rand()%MAX_STR_LEN;
	memset(param->str, 0, sizeof(param->str));
	for (int i = 0; i < param->len; ++i){
		int max = 123;
		int min = 33;
		param->str[i] = rand()%(max-min) + min;
	}
}

int main(int argc, char const *argv[]){
	struct udp_struct recvdmsg_udp;
	int UDP_socket, bytes_read;

	//создание UDP сокета
	UDP_socket = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in UDP_addr;
	UDP_addr.sin_family = AF_INET;
	UDP_addr.sin_port = htons(UDP_PORT1);
	UDP_addr.sin_addr.s_addr = htonl(INADDR_ANY);


	bind(UDP_socket, (struct sockaddr *)&UDP_addr, sizeof(UDP_addr));

	while(1)
		{
			if ((bytes_read = recvfrom(UDP_socket, &recvdmsg_udp, sizeof(recvdmsg_udp), 0, NULL, NULL)) > 0)
			{	
				if (recvdmsg_udp.message[0] == 'W')
				{
				//	printf("%s , bytes_read = %d\n Для вас выделен порт = %d\n", recvdmsg_udp.message, bytes_read, recvdmsg_udp.tcp_port);
					break;
				}	
			}
			sleep(1);

		}
	
	int yes=1;
	if (setsockopt(UDP_socket,SOL_SOCKET,SO_REUSEADDR|SO_REUSEPORT,&yes,sizeof(int)) == -1)
	{
	  perror("setsockopt");
	  exit(1);
	}
	close(UDP_socket);

	int TCP_PORT1 = recvdmsg_udp.tcp_port;

	//TCP общение
	struct msgbuf param;
	srand(time(NULL));

	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	struct sockaddr_in Sockaddr;
	Sockaddr.sin_family = AF_INET;
	Sockaddr.sin_port = htons(TCP_PORT1);
	Sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	connect(Socket, (struct sockaddr*)(&Sockaddr), sizeof(Sockaddr));

	int type = CLIENT_SENDER;
	send(Socket, &type, sizeof(int), MSG_NOSIGNAL);

	while(1){
		rng(&param);
		send(Socket, &param, sizeof(param), MSG_NOSIGNAL);
		printf("Клиент1 отправил сообщение!\n");
		sleep(param.time);
	}

	close(Socket);
	return 0;
}