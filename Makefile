run: client client2 server
	./server
server: server.o
	gcc -o server server.o -lpthread
client: client.o
	gcc -o client client.o
client2: client2.o
	gcc -o client2 client2.o
server.o: server.c
	gcc -std=c99 -c server.c
client.o: client.c
	gcc -std=c99 -c client.c
client2.o: client2.c
	gcc -std=c99 -c client2.c
clean:
	rm client client2 server client.o client2.o server.o
