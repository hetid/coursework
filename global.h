#ifndef GLOBAL_H
#define GLOBAL_H

#define TCP_PORT 1488
#define MAX_STR_LEN 256
#define MAX_TIME 10
#define MAX_QUEUE_LEN 10
#define DELAY_UDP 3
#define UDP_PORT1 2000
#define UDP_PORT2 3000


enum client_type_t {
	CLIENT_SENDER = 1,
	CLIENT_RECIEVER
};

struct msgbuf {
	int time;
	int len;
	char str[MAX_STR_LEN + 1];
};

struct udp_struct {
	char message[MAX_STR_LEN];
	int tcp_port;
};

#endif