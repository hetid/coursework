#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "global.h"

int main(int argc, char const *argv[]){
	struct udp_struct recvdmsg_udp;
	int UDP_socket, bytes_read = 0;
	
	//создание UDP сокета
	UDP_socket = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in UDP_addr;
	UDP_addr.sin_family = AF_INET;
	UDP_addr.sin_port = htons(UDP_PORT2);
	UDP_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(UDP_socket, (struct sockaddr *)&UDP_addr, sizeof(UDP_addr)) < 0)
	{
		perror("bind");
		sleep(3);
		return(-1);
	}

	//	printf("Ждёт коннекта\n");
	while(1)
		{
			if ((bytes_read = recvfrom(UDP_socket, &recvdmsg_udp, sizeof(recvdmsg_udp), 0, NULL, NULL)) > 0)
			{
				if (recvdmsg_udp.message[0] == 'M')
				{
					
				//	printf("%s , bytes_read = %d\n Для вас выделен порт = %d\n", recvdmsg_udp.message, bytes_read, recvdmsg_udp.tcp_port);
					break;
				}
			}
			sleep(1);
		}

	int yes=1;	
	if (setsockopt(UDP_socket,SOL_SOCKET,SO_REUSEADDR|SO_REUSEPORT,&yes,sizeof(int)) == -1)
	{
	  perror("setsockopt");
	  exit(1);
	}
	close(UDP_socket);

	int TCP_PORT2 = recvdmsg_udp.tcp_port;

	struct msgbuf param;
	//TCP socket
	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	struct sockaddr_in Sockaddr;
	Sockaddr.sin_family = AF_INET;
	Sockaddr.sin_port = htons(TCP_PORT2);
	Sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	//установка TCP соединения
	connect(Socket, (struct sockaddr*)(&Sockaddr), sizeof(Sockaddr));
	int type = CLIENT_RECIEVER;
	send(Socket, &type, sizeof(int), MSG_NOSIGNAL);

	while(1){
		recv(Socket, &param, sizeof(param), MSG_NOSIGNAL);
		printf("Клиент2 получил сообщение! \ntime = %d ,len = %d, %s\n", param.time, param.len, param.str);
		sleep(param.time);

	}

	close(Socket);
	return 0;
}